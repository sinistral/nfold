
(ns nfold.render-test
  (:use clojure.test)
  (:require [nfold.render :as render]))

(deftest test:hub-coordinates
  []
  (testing "hub-coordinates are returned based on the hub index"
    (with-redefs [render/origin (fn [] {:x 0 :y 0})
                  render/point-distance (fn [] 1)]
      (is (= {:x 0 :y 0} (render/hub-coordinates 0)))
      (is (= {:x 1 :y 0} (render/hub-coordinates 1)))
      (is (= {:x 1 :y 1} (render/hub-coordinates 2)))
      (is (= {:x 1 :y 2} (render/hub-coordinates 3)))
      (is (= {:x 0 :y 2} (render/hub-coordinates 4)))
      (is (= {:x 0 :y 1} (render/hub-coordinates 5)))))
  (testing "no co-ordinates are returned for invalid hub indices"
    (is (nil? (render/hub-coordinates 6)))))
