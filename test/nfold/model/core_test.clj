
(ns nfold.model.core_test
  (use clojure.test
       nfold.model.core))

(deftest test:make-initial-state
  []
  (let [test-pair (fn [source target]
                    (let [initial-state (make-initial-state source target)
                          transition (:digit-transition initial-state)
                          {:keys [on off]} transition
                          error (if (or (not (empty? on)) (not (empty? off)))
                                  nil
                                  {:source source :target target})]
                      (is (nil? error))))]
    (doseq [source (range 0 10)]
      (doseq [target (range 0 10)]
        (when-not (= source target)
          (test-pair source target))))))

(deftest test:select-spoke:deactivation
  []
  (testing "conventional deactivation; fold into an active neighbour"
    (is (= [0 1] (select-spoke:deactivation (spoke-activation-difference 7 1)
                                            [0 1 2]))))
  (testing "no immediate activated neighbours to deactivate; use neighbour that is to become active"
    (is (= [2 3] (select-spoke:deactivation (spoke-activation-difference 1 2)
                                            [2 1])))))

(deftest test:next-spoke-transition
  []
  (let [next (apply (let [changes (spoke-activation-difference 8 0)
                          spokes (spokes-for-digit 8)]
                      (next-spoke-transition changes spokes)) [{}])]
    (let [pair (set (:pair (:spoke-transition next)))]
      (is (pair 6))
      (is (or (pair 1) (pair 2) (pair 4) (pair 5))))
    (let [digit-trans (:digit-transition next)]
      (is (= {:off #{}, :on #{}} digit-trans)))))
