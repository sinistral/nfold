
(ns nfold.model.rotation-test
  (:use nfold.model.rotation)
  (:require [clojure.test :refer [deftest is testing]]))

(deftest test$quadrant-translation
  (let [tr (fn [q s]
             (doall (for [[a b] s]
                      (do (is (== b (q0->qn a q)))
                          (is (== a (qn->q0 b q)))))))
        [s0 s1 s2 s3 s4] [0 +half-pi+ +pi+ (+ +pi+ +half-pi+) (* 2 +pi+)]]
    (testing "'identity' quadrant translation (q0 -> q0) is a no-op"
      (tr 0 [[s0 s0] [s1 s1] [1 1]]))
    (testing "q1; range: pi/2 -> pi"
      (tr 1 [[s0 s1] [s1 s2] [1 (+ s1 1)]]))
    (testing "q2; range: pi -> (+ pi pi/2)"
      (tr 2 [[s0 s2] [s1 s3] [1 (+ s2 1)]]))
    (testing "q3; range: (+ pi pi/2) 2pi"
      (tr 3 [[s0 s3] [s1 s4] [1 (+ s3 1)]]))))

(deftest test$step
  (let [min 0]
    (testing "angle will not be decremented beyond its minimum value"
      (is (= min (step 0 9 :anticlockwise))))
    (testing "an angle not yet at it's minimum value will be decremented"
      (is (= min (step 1 1 :anticlockwise)))))
  (let [max +half-pi+]
    (testing "an angle will not be incremented beyond its maximum value"
      (is (= max (step 0 9 :clockwise))))
    (testing "an angle not yet at it's maximum value will be incremented"
      (is (= max (step (- +half-pi+ 1) 1 :clockwise))))))

(deftest test$transition
  (testing "a transition will be started if there is no pre-existing state"
    (let [expected {:index 0 :angle 0}]
      (doall (for [state [nil {}]]
               (is (= expected (transition 0 5 state)))))))
  (testing "a transition is continued if there is pre-existing state"
    (is (= {:index 0 :angle +rotation-interval+}
           (transition 0 5 {:angle 0})))
    (is (= {:index 0 :angle (* 2 +rotation-interval+)}
           (transition 0 5 {:angle +rotation-interval+}))))
  (testing "meta-data in the input state is included in the output state"
    (is (= {:index 0 :angle +rotation-interval+ :included? true}
           (transition 0 5 {:angle 0 :included? true})))))

(deftest test$candidates
  (testing "returns an empty list for an invalid spoke number"
    (is (= [] (candidates 9))))
  (testing "correctly returns all candidates for the central spoke"
    (is (= #{1 2 4 5} (apply hash-set (candidates 6))))))
