
(ns nfold.render
  (:import java.awt.Color
           java.awt.Dimension
           java.awt.Polygon
           java.awt.RenderingHints
           java.awt.geom.AffineTransform)
  (:require [clojure.tools.logging :refer [trace]]))

(def +spoke-length+ 180)
(def +spoke-width+ 20)

(defn ^:private make-polygon
  [points]
  (Polygon. (into-array Integer/TYPE (map first points))
            (into-array Integer/TYPE (map second points))
            (count points)))

(defn ^:private identity-spoke
  "Constructs a so-called 'identity' spoke, which is the polygon that has the
dimensions of the spoke, with one end at the origin and its length along the
postive x axis."
  [length width]
  (let [l (- length width)
        w (/ width 2)]
    (make-polygon [[0                 0]
                   [w             (- w)]
                   [(+ w l)       (- w)]
                   [(+ (* 2 w) l)     0]
                   [(+ w l)           w]
                   [w                 w]])))

(def +identity-spoke+ (identity-spoke +spoke-length+ +spoke-width+))

;; -------------------------------------------------------------------------- ;;

(defn hub-radius
  []
  (/ +spoke-width+ 4))

(defn point-distance
  []
  (+ (* 2 (hub-radius)) +spoke-length+))

(defn relative-point
  [ref axis adj-fn distance]
  (assoc ref axis (adj-fn (get ref axis) distance)))

(defn origin
  []
  {:x 50 :y 70})

(defn hub-coordinates
  [idx]
  (cond (= idx 0) (origin)
        (= idx 1) (relative-point (hub-coordinates 0) :x + (point-distance))
        (= idx 2) (relative-point (hub-coordinates 1) :y + (point-distance))
        (= idx 3) (relative-point (hub-coordinates 2) :y + (point-distance))
        (= idx 4) (relative-point (hub-coordinates 5) :y + (point-distance))
        (= idx 5) (relative-point (hub-coordinates 0) :y + (point-distance))))

;; -------------------------------------------------------------------------- ;;

(defn rotate-shape
  [shape radians]
  (trace "rotating" shape "by" radians)
  (.createTransformedShape (AffineTransform/getRotateInstance radians) shape))

(defn translate-shape
  [shape {x :x y :y}]
  (.createTransformedShape (AffineTransform/getTranslateInstance x y) shape))

(defmulti spoke (fn [hubspec-or-index rads] (map? hubspec-or-index)))

(defmethod spoke
  true [{{x :x y :y} :point r :radius} radians]
  (translate-shape (rotate-shape +identity-spoke+ radians)
                   {:x (+ x (* r (Math/cos radians)))
                    :y (+ y (* r (Math/sin radians)))}))

(defmethod spoke
  false [index radians]
  (spoke {:point (hub-coordinates index) :radius (hub-radius)} radians))

;; -------------------------------------------------------------------------- ;;

(defn inject-instance
  ([g forms]
     (inject-instance g forms []))
  ([g clean tainted]
     (if (empty? clean)
       tainted
       (recur g (rest clean) (conj tainted
                                   (let [f (first clean)]
                                     (concat (list (first f) g) (rest f))))))))

(defmacro with-frame-graphics
  [frame & forms]
  (let [graphics (gensym)
        strategy (gensym)
        tainted-forms (inject-instance graphics forms)]
    `(let [~strategy (.getBufferStrategy ~frame)
           ~graphics (.getDrawGraphics ~strategy)]
       (try ~@tainted-forms (finally (.dispose ~graphics)))
       (.show ~strategy))))

(defn clear-rectangle
  [graphics {w :width h :height}]
  (.clearRect graphics 0 0 w h))

(defn draw-8
  [g]
  (let [half-pi (/ Math/PI 2)
        spokes (concat (map #(spoke %1 0) [0 5 4])
                       (map #(spoke %1 half-pi) [0 1 5 2]))
        color (.getColor g)]
    (.setColor g Color/GRAY)
    (doseq [s spokes] (.draw g s))
    (.setColor g color)))

(defn set-graphics-options
  [g]
  (.addRenderingHints g {RenderingHints/KEY_ANTIALIASING
                         RenderingHints/VALUE_ANTIALIAS_ON})
  (.setColor g Color/WHITE)
  (.setBackground g Color/BLACK))

(defn visualise-model
  [g m]
  (let [nstate @(:nstate m)
        static (vals (:spokes nstate))
        transition (:spoke-transition nstate)]
    ;; Render the digit, but don't require a transition to be available.  It
    ;; is possible for none to be available when a new target digit is being
    ;; swapped in by the model between digit transitions.
    (doseq [{:keys [angle index]}
            (if transition (conj static transition) static)]
      (let [shape (spoke index angle)]
        (.draw g shape)
        (.fill g shape)))))

;; -------------------------------------------------------------------------- ;;

(defn render-frame
  [jframe model]
  (with-frame-graphics jframe
    (set-graphics-options)
    (clear-rectangle (bean (.getSize jframe)))
    (draw-8)
    (visualise-model model))
  (Thread/sleep 50))

(defn make-renderer
  [model jframe]
  {:engine {:running? (atom false)
            :cycle (fn [r] (render-frame jframe model))}})
