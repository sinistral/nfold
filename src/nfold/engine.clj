
(ns nfold.engine
  "In which is defined the methods for managing the life-cycle of an engine.
Engines can be `start`ed and `stop`ped.  When started a new `Thread` will be
created to run the engine, until such time as `stop` is called.

To be managed as an engine, a caller need only provide a map of the following
form:

   {:engine {:running? (atom false) :cycle (fn [e] ...)}}

The `cycle` function provides the motive power, so to speak.  The thread that
is started to run the engine does so by repeatedly calling this function.
Currently this function is expected to provide its own timing delays if it
doesn't want to be run in a tight loop.
The parameter to this function is the map itself.  Thus implementation are free
to record whatever state is needed by the `cycle` function in this structure.

The state of the engine (running or not) is tracked in the `running?` atom."
  (:require [clojure.tools.logging :refer [fatal]]))

(defn ^:private set-running
  [atom running?]
  (dosync (swap! atom (fn [x] running?))))

(defn start
  "Set an engine running by creating a new thread to repeatedly call the
function returned by `(get-in e [:engine :cycle])`."
  [e]
  (let [running? (get-in e [:engine :running?])
        cycle-fn (get-in e [:engine :cycle])]
    (when-not @running?
      (set-running running? true)
      (.start (proxy [Thread] []
                (run
                  []
                  (letfn [(-cycle-engine
                            []
                            (when @running?
                              (try (cycle-fn e)
                                   (catch Throwable ex
                                     (set-running running? false)
                                     (doto ex
                                       (fatal "cycle failed for engine" e)
                                       (throw))))
                              (recur)))]
                    (-cycle-engine))))))
    e))

(defn stop
  "Indicate to an engine that it should stop running.  The thread running
the engine will exit on the next return of the `cycle` function."
  [e]
  (dosync (swap! (get-in e [:engine :running?]) (fn [x] false)))
  e)
