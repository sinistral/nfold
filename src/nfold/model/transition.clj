
(ns nfold.model.transition
  (require [nfold.model.rotation :as rotation]))

(defmulti transition (fn [type source target spec] type))

(defmethod transition :rotation
  [_ source target state]
  (rotation/transition source target state))

(defmulti candidates (fn [type spoke] type))

(defmethod candidates :rotation
  [_ spoke]
  (rotation/candidates spoke))
