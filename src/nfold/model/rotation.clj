
(ns nfold.model.rotation
  "In which is defined the structures and functions to manage the rotation of
a digit spoke about its hub(s).  The rotation of a spoke is described in terms
of rotating one in a pair of spokes towards its peer, within the so-called
quadrant in which it falls.  Quadrants are numbered as follows:

                                   2 | 3
                                  ---+---
                                   1 | 0

Quandrant 0 matches the positive number space in the Java AWT graphics
co-ordinate system, and the numbering proceeds clockwise.  In their default
positions (as they would be when forming the number 8, for example), spoke pairs
form the axes of a quadrant.  Given the standard model spoke numbering:

                                     0
                                    ---
                                 5 | 6 | 1
                                    ---
                                 4 | 3 | 2
                                    ---

the rotation of spokes #{0 5} would occur in quadrant 0, while the rotation of
spokes #{2 3} would occur in quadrant 2.  Please note that consumers should not
be aware of the quadrant numbering.  All rotations are described in terms of
moving one spoke in the direction of a peer; that is 0->5, 6->1, 6->4, etc.")

(def +pi+ Math/PI)

(def +half-pi+ (/ +pi+ 2))

(def +rotation-interval+ (/ +half-pi+ 50))

(def ^:private +rotation-specs+
  "Contains descriptions of the rotations for the spokes that make up a digit.
Descriptions are of the form: `{:clockwise [x y] :quad n :index i}`, where
`:clockwise` describes the order in which the pairs of the spokes would be
encountered when moving in a clockwise direction about their shared hub (which
is given by `:index`), and `:quad` is the quadrant in which they fall."
  (loop [ordered {[0 5] {:quad 0 :index 0}
                  [1 0] {:quad 1 :index 1}
                  [3 2] {:quad 2 :index 3}
                  [4 3] {:quad 3 :index 4}
                  [5 6] {:quad 3 :index 5}
                  [6 1] {:quad 2 :index 2}
                  [2 6] {:quad 1 :index 2}
                  [6 4] {:quad 0 :index 5}}
         paired {}]
    (if (empty? ordered)
      paired
      (let [[[x y :as order] spec] (first ordered)]
        (recur (rest ordered)
               (assoc paired #{x y} (assoc spec :clockwise order)))))))

(defn rotation-spec
  "Returns the rotation spec for a given pair (described by a 2-element set,
e.g. #{0 1}, and throws an exception if the pair is invalid."
  [pair]
  (or (get +rotation-specs+ pair)
      (throw (ex-info
              (str "invalid rotation; no data found for pair " pair)
              {:pair pair}))))

(def ^:private +quad-offsets+ {0 0
                               1 +half-pi+
                               2 +pi+
                               3 (+ +pi+ +half-pi+)})

(defn ^:private quad-offset
  [n]
  (or (get +quad-offsets+ n)
      (throw (ex-info (str "invalid quad " n) {:quad n}))))

(defn qn->q0
  [angle quad]
  (- angle (quad-offset quad)))

(defn q0->qn
  [angle quad]
  (+ angle (quad-offset quad)))

(defn step
  "Compute the next value for an angle, checking, and clamping to boundary
conditions.  This function assumes that the input angle is in quadrant 0 (that
is: in the range (0, pi/2) and performs bounds checking accordingly.  To advance
angles from other quadrants, they should first be translated to quadrant 0, and
translation back to the appropriate quadrant after they have been advanced.
cf. `qn->q0` and `q0-qn`."
  [angle increment direction]
  (let [[next exceeds bound] (if (= direction :clockwise)
                               [+ > +half-pi+]
                               [- < 0])
        next-angle (apply next [angle increment])]
    (if (exceeds next-angle bound) bound next-angle)))

(defn initial-angle
  [quad direction]
  (if (= direction :clockwise)
    (q0->qn 0 quad)
    (q0->qn +half-pi+ quad)))

;; -------------------------------------------------------------------------- ;;

(defn candidates
  "Return a list of candidate spokes to which the given spoke could be
rotated."
  [spoke]
  (map (fn [e] (first (disj (first e) spoke)))
       (filter (fn [s] (contains? (first s) spoke))
               +rotation-specs+)))

(defn transition
  [source target state]
  (let [spec (rotation-spec #{source target})
        {quad :quad index :index cw-pair :clockwise} spec
        {:keys [angle]} state
        direction (if (= [source target] cw-pair) :clockwise :anticlockwise)]
    (if angle
      (-> state
          (assoc :angle (-> angle
                            (qn->q0 quad)
                            (step +rotation-interval+ direction)
                            (q0->qn quad)))
          ;; Add the index that we *know* to be correct for the quad, just
          ;; in case the input map has been corrupted in some way.
          (assoc :index index))
      (merge state {:index index :angle (initial-angle quad direction)}))))
