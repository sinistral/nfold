
(ns nfold.model.core
  (:require [clojure.set :as set]
            [clojure.tools.logging :refer [debug info]]
            [nfold.model.transition :refer [candidates transition]]))

(def +spoke-specs+
  "Describes the parameters to place each spoke at its default \"rest\"
position."
  (let [h 0 v (/ Math/PI 2)]
    {0 {:index 0 :angle h}
     1 {:index 1 :angle v}
     2 {:index 2 :angle v}
     3 {:index 4 :angle h}
     4 {:index 5 :angle v}
     5 {:index 0 :angle v}
     6 {:index 5 :angle h}}))

(def +digit-specs+
  "Describes, for each digit, the spokes that must be turned on for that
number to be displayed."
  {0 #{0 1 2 3 4 5}
   1 #{1 2}
   2 #{0 1 6 4 3}
   3 #{0 1 6 2 3}
   4 #{5 6 1 2}
   5 #{0 5 6 2 3}
   6 #{0 5 4 6 2 3}
   7 #{0 1 2}
   8 #{0 1 2 3 4 5 6}
   9 #{6 5 0 1 2 3}})

(def +spoke-adjacency+
  {0 [1 5] 1 [0 6] 2 [3 6] 3 [2 4] 4 [3 6] 5 [0 6] 6 [1 2 4 5]})

(defn spokes-for-digit
  [n]
  (loop [indices (get +digit-specs+ n)
         spokes {}]
    (if-not (empty? indices)
      (recur (rest indices) (let [index (first indices)]
                              (assoc spokes index (get +spoke-specs+ index))))
      spokes)))

(defn spoke-activation-difference
  "Determine the difference between the spoke activation states of the source
and target digits.  The term 'activation state' refers to whether a spoke
should be on or off when expressing a given digit.  The difference is expressed
as a map with the keys `:off` and `:on`, which represent desired states and
which map to the set of spokes that should be in that state."
  [source target]
  (let [source-spec (get +digit-specs+ source)
        target-spec (get +digit-specs+ target)]
    {:off (set/difference source-spec target-spec)
     :on (set/difference target-spec source-spec)}))

(defn select-spoke:deactivation
  [{:keys [on off]} spokes]
  ;; First find all spokes whose neighbours are also either going to be
  ;; deactivated, or are already deactivated.  These must be folded into one
  ;; of their _active_ neighbours.  We look for these 'isolated' spokes, and
  ;; transition them first to avoid actually creating islands out of them
  ;; (i.e. not having an active neighbour into which to fold them.
  (let [is-inactive (set/difference (get +digit-specs+ 8) spokes)
        to-be-inactive (set (into (seq off) is-inactive))
        inactive-neighbours? #(every? to-be-inactive (get +spoke-adjacency+ %))
        isolated (seq (filter inactive-neighbours? off))
        source (if isolated
                 ;; TODO: randomise selection
                 (first isolated)
                 (first off))]
    ;; Return the source and target for the translation.   The target is one
    ;; of the source's active neighbours.
    (let [target-candidates (into spokes on)]
      [(or source
           (throw (ex-info "no spokes eligible for deactivation"
                           {:candidates off :spokes spokes})))
       (or (some (set (get +spoke-adjacency+ source)) target-candidates)
           (throw (ex-info "no suitable neighbours found for deactivation transition"
                           {:candidates off :spokes target-candidates})))])))

(defn select-spoke:activation
  [{:keys [on off]} spokes]
  (let [active-neighbour #(some (set (get +spoke-adjacency+ %)) spokes)
        activation-pairs (reduce (fn [vec target]
                                   (if-let [source (active-neighbour target)]
                                     (conj vec [source target])
                                     vec))
                                 (into [[]] on))]
    (when (empty? activation-pairs)
      (throw (ex-info "no spokes are eligible for activation"
                      {:candidates on :spokes spokes})))
    ;; TODO: randomise selection
    (first activation-pairs)))

(defn next-spoke-transition
  [{:keys [on off] :as candidates} spokes]
  (let [indices (keys spokes)]
    (cond (not (empty? off))
          (let [[source target] (select-spoke:deactivation candidates indices)]
            #(-> %
                 (assoc :spokes (dissoc (:spokes %) source))
                 (assoc :spoke-transition {:pair [source target]})
                 (assoc :digit-transition (merge (:digit-transition %)
                                                 {:off (set (remove #{source} off))
                                                  :on (set (remove #{target} on))}))))
          (not (empty? on))
          (let [[source target] (select-spoke:activation candidates indices)]
            #(-> %
                 (assoc :spoke-transition {:pair [source target]})
                 (assoc :digit-transition (merge (:digit-transition %)
                                                 {:on (set (remove #{target} on))
                                                  :off off})))))))

(defn continue-state-transition
  [state]
  (let [spoke-trans (:spoke-transition state)
        [source target] (:pair spoke-trans)]
    (when spoke-trans
      (let [next-state (transition :rotation source target spoke-trans)]
        (when-not (= (:angle next-state) (:angle spoke-trans))
          (assoc state :spoke-transition next-state))))))

(defn finalize-previous-transition
  "Makes the target of the previous transition permanent; that is: the target
is added to the list of spokes that are considered to be 'active'."
  [current]
  (if-let [[_ target] (:pair (:spoke-transition current))]
    (assoc-in current [:spokes target] (get +spoke-specs+ target))
    current))

(defn make-initial-state
  [source target]
  {:spokes (spokes-for-digit source)
   :digit-transition (-> (spoke-activation-difference source target)
                         (merge {:source source :target target}))})

(defn transition-ended?
  [state]
  (let [{:keys [on off]} state]
    (and (empty? on) (empty? off))))

(defn next-state-transition
  [current]
  (let [state (finalize-previous-transition current)
        transition (:digit-transition state)]
    (if (transition-ended? transition)
      (let [cur-target (:target transition)
            new-target (get state :input)]
        (if (and new-target (not (= cur-target new-target)))
          (recur (or (make-initial-state cur-target new-target) state))
          state))
      (continue-state-transition
       (let [spokes (:spokes state)
             digit-trans (:digit-transition state)
             next-state ((next-spoke-transition digit-trans spokes) state)]
         (debug "constructed state for next transition:" next-state)
         next-state)))))

(defn advance-state
  [current]
  (or (continue-state-transition current) (next-state-transition current)))

(defn make-model
  []
  (let [nstate (atom (next-state-transition (make-initial-state 8 8)))]
    {:nstate nstate
     :engine {:running? (atom false)
              :cycle (fn [m]
                       (dosync (swap! nstate #(advance-state %1)))
                       (Thread/sleep 50))}
     :input {:handler (fn [x]
                        (when (transition-ended? (:digit-transition @nstate))
                          (let [{:keys [_ target]} (:digit-transition @nstate)]
                            (dosync
                             (swap! nstate #(assoc % :input x)))
                            (debug "updated model state:" nstate))))}}))
