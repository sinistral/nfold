
(ns nfold.core
  (:import java.awt.Dimension
           java.awt.event.KeyAdapter
           javax.swing.JFrame)
  (:require [nfold.engine :refer [start stop]]
            [nfold.render :refer [make-renderer]]
            [nfold.model.core :refer [make-model]]))

(defn make-numeric-key-listener
  [model]
  (proxy [KeyAdapter]
      []
    (keyTyped [key-event]
      (let [value (.getKeyChar key-event)]
        (when (Character/isDigit value)
          ((:handler (:input model)) (Integer. (str value))))))))

(defn make-frame
  [key-listener]
  (doto (JFrame.)
    (.setDefaultCloseOperation JFrame/DISPOSE_ON_CLOSE)
    (.setSize (Dimension. 300 500))
    (.setVisible true)
    (.createBufferStrategy 2)
    (.addKeyListener key-listener)))

(defn init
  []
  (let [model (make-model)
        frame (make-frame (make-numeric-key-listener model))
        renderer (make-renderer model frame)]
    {:model model :renderer renderer}))

;; -------------------------------------------------------------------------- ;;

(def *engines* nil)

(defn engines
  [action-fn]
  (map action-fn (map second *engines*)))
